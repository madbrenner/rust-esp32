# esp32-rust-container

This image provides an environment for cross compiling Rust projects to run on an ESP32 device.
Originally I attempted to use [mtnmts's](https://github.com/mtnmts/rust-esp32-container) container, but ran into some issues with using Xargo. Taking some inspiration from that I built my own image that is significantly smaller at about 2.4GB, acheived by building Rust in one layer, then starting from a fresh image and copying over just the necessary output.
A few common use cases are easily supported by command line parameters when running this container, provided by the fact that the container by default runs the startup.sh bash script provided here.

# Usage

To build your project, run this container with your Rust code mounted to the /code directory in the image, and provide the -b parameter.
```
docker run -v PATH_TO_RUST_CODE:/code madbrenner/rust-esp32:latest -b
```

In addition, this container supports converting the ELF binary output into an image file that can be flashed to an ESP32 device.
```
docker run -v PATH_TO_RUST_CODE:/code madbrenner/rust-esp32:latest -i
```

If you want to do it all, you can also directly flash the output image to an attached ESP32 device, though to accomplish this you will need to pass the serial device through to the container, if your ESP32 was mounted at /dev/ttyUSB0 the below command would flash your Rust project to it.
```
docker run --device /dev/ttyUSB0 -v PATH_TO_RUST_CODE:/code madbrenner/rust-esp32:latest -f
```

By default the container will assume your ESP32 device is mounted at /dev/ttyUSB0, but if it is at another location, you can tell the container which device to use with the -d parameter.
```
docker run --device /dev/ttyUSB1 -v PATH_TO_RUST_CODE:/code madbrenner/rust-esp32:latest -f -d /dev/ttyUSB1
```

If these canned operations are insufficient for your needs you can also tell the container to run an arbitrary command you provide with the -c parameter
```
docker run -v PATH_TO_RUST_CODE:/code madbrenner/rust-esp32:latest -c 'echo "This code will be executed by $(whoami) on $(hostname)"; echo "As will this"'
```

You can also run an interactive session of the container by providing the `-it --entrypoint bash` parameters to the docker command.
```
docker run -v PATH_TO_RUST_CODE:/code -ti --entrypoint bash madbrenner/rust-esp32:latest
```

You can also access the help information from the startup.sh script by providing it with the -h parameter.
```
docker run madbrenner/rust-esp32:latest -h
```
